#!/usr/bin/env python3

import subprocess
import os
import re

variable_assignment_pattern = r'\w+\s+=\s+\w+'
print_pattern = r'print\(\w+\)'


working_directory = os.getcwd()
print('Current working directory: {}'.format(working_directory))

def run_command(command):

    """Runs a command using subprocess module"""

    subprocess.call(command.split(' '), cwd=working_directory)


accepted_commands = ['cd', 'echo', 'exit', 'ls', 'mkdir', 'pwd', 'rm', 'rmdir', 'ssh', 'vim']

print('Welcome to version 0.0.0.1 of PyBash!')
print('Accepted commands are: {}'.format(' '.join(accepted_commands)))

while True:

    read_input = input("pybash > ")

    # print('Command part: {}'.format(read_input.split(' ')[0]))

    if read_input.split(' ')[0] == 'cd':
        working_directory = read_input.split(' ')[1]
        print('Current working directory: {}'.format(working_directory))
    elif read_input == 'exit':
        print('Bye!!')
        exit(0)
    elif re.match(variable_assignment_pattern, read_input):
        exec(read_input)
        variable, value = read_input.split(' = ')
        print('{} assigned to {}'.format(value, variable))
    elif re.match(print_pattern, read_input):
        exec(read_input)
    elif read_input.split(' ')[0] in accepted_commands:
        run_command(read_input)
    else:
        print('Please try a valid command..')
