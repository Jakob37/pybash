# README #

This project will end up as the perfect mix between the Bash shell interactivity and the Python syntax awesomeness and scalability.

Currently supported commands (feel free to add flag arguments):

* cd (only absolute paths)
* echo
* exit
* ls
* mkdir
* pwd
* rm
* rmdir
* ssh
* vim

Working Python implementations:

* Assign value to variable
* Print value of variable

Is it even possible?

* Tab completion